// Variablendeklarationen
var ausdruck = "";
var schritt = 0;
var operatoren = ['+', '-', '*', '/'];
var zahlen = [1, 2, 3, 4, 5, 6, 7, 8, 9];

// GUI-Generierung (Buttons, Texte, Textbox)
function setup() {
  createCanvas(1000, 1000);
  labelLaenge = createElement('h3', 'Länge des zufälligen Ausdrucks:');
  labelLaenge.position(30, 0);

  input = createInput();
  input.position(labelLaenge.x + 270, labelLaenge.y + 20);

  buttonSubmit = createButton('Generieren');
  buttonSubmit.position(labelLaenge.x, 50);
  buttonSubmit.mousePressed(ausdruckGenerieren);
  
  buttonNextStart = createButton('Erster Schritt');
  buttonNextStart.position(labelLaenge.x + buttonSubmit.width, 50);
  buttonNextStart.mousePressed(nextStart);
  
  buttonNext = createButton('Nächster Schritt');
  buttonNext.position(labelLaenge.x + buttonSubmit.width + buttonNextStart.width, 50);
  buttonNext.mousePressed(next);

  stroke(255);
  textSize(20);
  fill(255, 255, 255);

  fill(0, 0, 0);
  textSize(12);
  text("Anleitung:", input.x + input.width + 50, 17);
  text("Generieren: Generiert einen zufälligen Ausdruck", input.x + input.width + 50, 37);
  text("Erster Schritt: Startzustand für die schrittweise Generierung eines Ausdrucks", input.x + input.width + 50, 57);
  text("Nächster Schritt: Nächster Zustand des schrittweise generierten Ausdrucks", input.x + input.width + 50, 77);
  textSize(20);
  text("A", 30, 120);
  markState(".", ".");
}

function draw() {

}

// Falls auf den Button "Generieren" geklickt wird,
// wird diese Funktion ausgeführt.
// Der Ausdruck wird durch eine zufällige Auswahl aus
// den beiden Arrays für die Operanden und Zahlen ganz 
// oben generiert, indem die Schleife in der Funktion
// so oft, wie die eingegebene Zahl in der Textbox, durchläuft.
async function ausdruckGenerieren(){
  if(input.value() <= 0){
    return;
  }
  ausdruck = "";
  var laenge = input.value();
  var randZ = Math.floor(Math.random()*zahlen.length);
  var randO = Math.floor(Math.random()*operatoren.length);
  var valZ = zahlen[randZ];
  var valO = operatoren[randO];
  
  ausdruck = ausdruck + valZ;
  fill(255, 255, 255);
  rect(20, 230, 1000, 1000);
  fill(0, 0, 0);
  text(ausdruck, 30, 250);
  markState("", valZ);

  for(var i = 2; i <= laenge; i++){
    await sleep(1000);
    randZ = Math.floor(Math.random()*zahlen.length);
    randO = Math.floor(Math.random()*operatoren.length);
    valZ = zahlen[randZ];
    valO = operatoren[randO];

    if(ausdruck.includes("\n-> ")){
      ausdruck = ausdruck + "\n-> " + "(" + split(ausdruck, "\n-> ")[i-2] + valO + valZ + ")";
    } else{
      ausdruck = ausdruck + "\n-> " + "(" + ausdruck + valO + valZ + ")";
    }
    fill(255, 255, 255);
    rect(20, 230, 1000, 1000);
    fill(0, 0, 0);
    text(ausdruck, 30, 250);
    markState(valO, valZ);
  }
}

// Wird ausgeführt, wenn auf "Erster Schritt" geklickt wird.
// Dabei werden alle benötigten Startwerte für die schrittweise
// Generierung eines Ausdrucks gesetzt, damit mit dem Button
// "Nächster Schritt" die nächsten Schritte korrekt ausgeführt werden.
function nextStart(){
  if(input.value() <= 0){
    return;
  }
  schritt = 2;
  var randZ = Math.floor(Math.random()*zahlen.length);
  var valZ = zahlen[randZ];
  ausdruck = "" + valZ;

  fill(255, 255, 255);
  rect(20, 230, 1000, 1000);
  fill(0, 0, 0);
  text(ausdruck, 30, 250);
  markState(".", valZ);
}

// Wird ausgeführt, wenn auf "Nächster Schritt" geklickt wird.
// Mithilfe der Variablen, sowie Arrays ganz oben, wird der 
// nächste, zufällige Schritt generiert.
function next(){
  var laenge = input.value();
  var randZ = Math.floor(Math.random()*zahlen.length);
  var randO = Math.floor(Math.random()*operatoren.length);
  var valZ = zahlen[randZ];
  var valO = operatoren[randO];

  if(schritt <= laenge){
    randZ = Math.floor(Math.random()*zahlen.length);
    randO = Math.floor(Math.random()*operatoren.length);
    valZ = zahlen[randZ];
    valO = operatoren[randO];

    if(ausdruck.includes("\n-> ")){
      ausdruck = ausdruck + "\n-> " + "(" + split(ausdruck, "\n-> ")[schritt-2] + valO + valZ + ")";
    } else{
      ausdruck = ausdruck + "\n-> " + "(" + ausdruck + valO + valZ + ")";
    }
    fill(255, 255, 255);
    rect(20, 230, 1000, 1000);
    fill(0, 0, 0);
    text(ausdruck, 30, 250);
    markState(valO, valZ);
  }
  schritt++;
}

// Parameter
// operand: Der aktuell ausgewählte Operand
// zahl: Die aktuell ausgewählte Zahl
// 
// Färbt den aktuellen Operanden und die aktuelle Zahl grün.
function markState(operand, zahl){
  stroke(255);
  fill(255, 255, 255);
  rect(25, 130, 400, 30);
  rect(25, 160, 400, 30);
  fill(0, 0, 0);

  text("O → ", 28, 150);
  if(operand == '+'){
    fill(0, 200, 0);
    text("+", 80, 150);
    fill(0, 0, 0);
  } else text("+", 80, 150);
  text("|", 96, 150);
  if(operand == '-'){
    fill(0, 200, 0);
    text("-", 110, 150);
    fill(0, 0, 0);
  } else text("-", 110, 150);
  text("|", 126, 150);
  if(operand == '*'){
    fill(0, 200, 0);
    text("*", 140, 150);
    fill(0, 0, 0);
  } else text("*", 140, 150);
  text("|", 156, 150);
  if(operand == '/'){
    fill(0, 200, 0);
    text("/", 170, 150);
    fill(0, 0, 0);
  } else text("/", 170, 150);

  text("Z → ", 31, 180);
  if(zahl == 0){
    fill(0, 200, 0);
    text("0", 80, 180);
    fill(0, 0, 0);
  } else text("0", 80, 180);
  text("|", 97, 180);
  if(zahl == 1){
    fill(0, 200, 0);
    text("1", 110, 180);
    fill(0, 0, 0);
  } else text("1", 110, 180);
  text("|", 127, 180);
  if(zahl == 2){
    fill(0, 200, 0);
    text("2", 140, 180);
    fill(0, 0, 0);
  } else text("2", 140, 180);
  text("|", 157, 180);
  if(zahl == 3){
    fill(0, 200, 0);
    text("3", 170, 180);
    fill(0, 0, 0);
  } else text("3", 170, 180);
  text("|", 187, 180);
  if(zahl == 4){
    fill(0, 200, 0);
    text("4", 200, 180);
    fill(0, 0, 0);
  } else text("4", 200, 180);
  text("|", 217, 180);
  if(zahl == 5){
    fill(0, 200, 0);
    text("5", 230, 180);
    fill(0, 0, 0);
  } else text("5", 230, 180);
  text("|", 247, 180);
  if(zahl == 6){
    fill(0, 200, 0);
    text("6", 260, 180);
    fill(0, 0, 0);
  } else text("6", 260, 180);
  text("|", 277, 180);
  if(zahl == 7){
    fill(0, 200, 0);
    text("7", 290, 180);
    fill(0, 0, 0);
  } else text("7", 290, 180);
  text("|", 307, 180);
  if(zahl == 8){
    fill(0, 200, 0);
    text("8", 320, 180);
    fill(0, 0, 0);
  } else text("8", 320, 180);
  text("|", 337, 180);
  if(zahl == 9){
    fill(0, 200, 0);
    text("9", 350, 180);
    fill(0, 0, 0);
  } else text("9", 350, 180);
  stroke(255)
}

// Hilfsfunktion, um eine bestimmte Zeit die Ausführung
// einer Funktion anzuhalten.
function sleep(millisecondsDuration)
{
  return new Promise((resolve) => {
    setTimeout(resolve, millisecondsDuration);
  })
}
